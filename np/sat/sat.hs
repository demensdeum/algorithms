{- SAT
  Haskell functions: and, or, not
  1. Wicky and Mindy wants to go to cinema together.
  2. Wicky, Mindy, Stacy, wants to go to cinema, but Stacy do not want to go with Mindy.
  3. Jeffrey wants to go to cinema with girls, but he hates Wicky, make formula for that case. 2 girls minimum in company.
-}

wicky = True
mindy = True
stacy = True
jeffrey = True

-- 1
firstResult = and[wicky, mindy]

-- 2
secondResult = and[not(and[stacy, mindy]), wicky]

-- 3
thirdResult = and[jeffrey, secondResult, not(and[wicky, jeffrey])]

main = do
  print firstResult
  print secondResult
  print thirdResult
