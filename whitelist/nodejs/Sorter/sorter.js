var fs = require('fs')

fs.readFile("../../resources/whitelist.json", 'utf8', function(error, data) {
    if (error) {
        throw error;
    }
    var whiteListContainer = JSON.parse(data);
    var whiteList = whiteListContainer.whiteList;
    
    var whiteListSorted = [...whiteList].sort(function(lhs, rhs){ return lhs.localeCompare(rhs) });

    whiteListContainer.whiteListSorted = whiteListSorted;
    
    fs.writeFile("../../resources/whitelist.json", JSON.stringify(whiteListContainer), 'utf8', function (error) {
        if (error) {
            throw error;
        }
    })
});
