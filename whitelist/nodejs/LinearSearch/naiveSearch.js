if (process.argv.length < 3) {
    console.log("Usage: node naiveSearch.js [search word]")
    console.log("Example: node naiveSearch.js demensdeum@gmail.com")
    process.exit(1);
}

var fs = require('fs')

fs.readFile("../../resources/whitelist.json", 'utf8', function(error, data) {
    if (error) {
        throw error;
    }
    var whiteListContainer = JSON.parse(data);
    search(searchWord, whiteListContainer.whiteList);
});

var searchWord = process.argv[2];

function search(word, whiteList) {  
    whiteList.forEach(whiteListWord => {
        if (whiteListWord === word) {
            console.log("Found")
            console.log(word)
            return;
        }
    })
}
