if (process.argv.length < 3) {
    console.log("Usage: node naiveSearch.js [search word]")
    console.log("Example: node naiveSearch.js demensdeum@gmail.com")
    process.exit(1);
}

var fs = require('fs')

fs.readFile("../../resources/whitelist.json", 'utf8', function(error, data) {
    if (error) {
        throw error;
    }
    var whiteListContainer = JSON.parse(data);
    var whiteListSorted = whiteListContainer.whiteListSorted;
    
    search(searchWord, whiteListSorted, 0, whiteListSorted.length - 1);
});

var searchWord = process.argv[2];

function search(word, sortedWhitelist, lowIndex, highIndex) {
    while (lowIndex <= highIndex) {
        
        var middleIndex = Math.floor((lowIndex + highIndex) / 2); 
        var middleWhitelistedWord = sortedWhitelist[middleIndex];        
        var compareResult = middleWhitelistedWord.localeCompare(word);
        
        if (compareResult == 0) {
             console.log("Found");
             console.log(word);
            return;            
        }        
        if (compareResult < 0) {
            lowIndex = middleIndex + 1;
        }
        else {
            highIndex = middleIndex - 1;
        }
    }
} 
