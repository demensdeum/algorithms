import Foundation

let url = URL(fileURLWithPath: "../../resources/whitelist.json")
let data = try? Data(contentsOf: url)
let json = try? JSONDecoder().decode([String : [String]].self, from: data!)

let sortedWhitelist = json!["whiteListSorted"]!
    
func search(_ word: String, _ sortedWhitelist: [String], _ lowIndex: Int, _ highIndex: Int) {
    var highIndex = highIndex
    var lowIndex = lowIndex

    while (lowIndex <= highIndex) {
        let middleIndex = (lowIndex + highIndex) / 2
        let middleWhitelistedWord = sortedWhitelist[middleIndex]
        let compareResult = middleWhitelistedWord.compare(word).rawValue
        
        if (compareResult == 0) {
             print("Found")
             print(word)
            return;            
        }        
        else if (compareResult < 0) {
            lowIndex = middleIndex + 1
        }
        else {
            highIndex = middleIndex - 1
        }
    }
}    
    
search(CommandLine.arguments[1], sortedWhitelist, 0, sortedWhitelist.count - 1)
