import Foundation

let url = URL(fileURLWithPath: "../../resources/whitelist.json")
let data = try? Data(contentsOf: url)
let json = try? JSONDecoder().decode([String : [String]].self, from: data!)

let whiteList = json?["whiteList"]

func search(_ word: String) {
    whiteList?.forEach {
        if $0 == word {
            print("Found")
            print(word)
            return;
        }
    }
}

search(CommandLine.arguments[1])
