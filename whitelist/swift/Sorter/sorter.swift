import Foundation

func lexiCompare(_ lhs: String, _ rhs: String) -> Int {
    let lhs = Array(lhs)
    let rhs = Array(rhs)
    
    for i in 0...lhs.count {
        if i >= rhs.count {
            return -1
        }
        let lhsSymbol = lhs[i]
        let rhsSymbol = rhs[i]
        
        if lhsSymbol < rhsSymbol {
            return -1
        }
        else if lhsSymbol > rhsSymbol {
            return 1
        }
        else {
            continue
        }
    }
    
    return 0
}

let url = URL(fileURLWithPath: "../../resources/whitelist.json")
let data = try? Data(contentsOf: url)
var json = try? JSONDecoder().decode([String : [String]].self, from: data!)

let whiteList = json!["whiteList"]!
print(whiteList)

var whiteListSorted = whiteList.sorted(by: { lexiCompare($0, $1) < 0 });
//var whiteListSorted = whiteList.sorted(by: { $0.compare($1).rawValue < 0 });
print(whiteListSorted)

json!["whiteListSorted"] = whiteListSorted

let jsonData = try JSONEncoder().encode(json)
print(jsonData)

try? jsonData.write(to: url)
