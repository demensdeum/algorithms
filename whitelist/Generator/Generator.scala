import scala.util.Random
import scala.collection.mutable.ListBuffer

class Generator {
    def generateRandomString(wordsCount: Int, wordLength: Int, wordToSearch: String): Array[String] = {
        var wordsList = new ListBuffer[String]()
        for(w <- 1 to wordsCount){
            var string = Random.alphanumeric.filter(_.isLetter).take(wordLength).mkString("")
            //string += "@"
            //string += Random.alphanumeric.filter(_.isLetter).take(wordLength).mkString("")
            //string += ".com"
            
            wordsList += string
        }
        
        wordsList += wordToSearch
        
        val shuffled = Random.shuffle(wordsList)
        
        return shuffled.toArray
    }
}

object GeneratorApp {
  def main(args: Array[String]): Unit = {
  
    val words = new Generator().generateRandomString(args(0).toInt, args(1).toInt, args(2))
    val wordsSorted = words.sortWith((lhs: String, rhs: String) => lhs.compareTo(rhs) < 0 )
    val json = words.mkString("\",\"")
    
    var output = "{"
    
    output += "\"whiteList\":[\""
    output += json
    output += "\"]"
    
    output += "}"
  
    println(output)
  }
}
