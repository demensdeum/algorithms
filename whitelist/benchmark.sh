if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ] || [ -z "$5" ]
  then
    echo "Usage: benchmark.sh [directory] [number of runs] [word to search] [whiteList generator words count] [whiteList generator word length]"
    echo "Example: benchmark.sh swift 10 demensdeum@gmail.com 10 10"
    echo "Example: benchmark.sh nodejs 10 demensdeum@gmail.com 10 10"
    exit 1
fi

echo "Preparing"

cd Generator
./build.sh
cd ..

cd $1
./build.sh
cd ..

RUNS_FROM=1
RUNS_TO=$2

echo "---"

performBenchmarking () {
    echo "Benchmarking $1"
    
    RUNTIMES=0
    
    for (( i=$RUNS_FROM; i<=$RUNS_TO; i++ ))
    do
    echo -ne "$i/$RUNS_TO\r"
    cd Generator
    ./generateWhiteList.sh $3 $4 $2
    cd ..
    cd $5
    cd Sorter
    ./run.sh
    cd ..
    cd $1
    start=`date +%s%N | cut -b1-13`        
    #./run.sh $2
    ./run.sh $2 | grep "Found" &> /dev/null
    end=`date +%s%N | cut -b1-13`
    if [ $? != 0 ]; then
        echo "Match not found - broken algorithm"
        exit 1
    fi
    runtime=$(((end-start)/$RUNS_TO))
    RUNTIMES=($RUNTIMES+$runtime)
    cd ..
    cd ..
    done
    averageRuntime=$(($RUNTIMES/$RUNS_TO))
    echo ""
    echo "Average: $averageRuntime"
    echo "---"
}

performBenchmarking "LinearSearch" $3 $4 $5 $1
performBenchmarking "BinarySearch" $3 $4 $5 $1
