import random
import time
from doubleSelectionSort import DoubleSelectionSort 

randomNumbers = [random.randrange(1, 200) for i in range(20000)]

numbers = randomNumbers.copy()
startTime = time.perf_counter()
sortedNumbers = sorted(numbers)
endTime = time.perf_counter()

numbers = randomNumbers.copy()
doubleSelectionStartTime = time.perf_counter()
doubleSelectionSortedNumbers = DoubleSelectionSort.sort(numbers, len(numbers))
doubleSelectionEndTime = time.perf_counter()

outputText = ""
for sortedNumber in sortedNumbers:
    outputText += f"{sortedNumber},"

#print(outputText)

doubleSortedOutputText = ""
for sortedNumber in doubleSelectionSortedNumbers:
    doubleSortedOutputText += f"{sortedNumber},"
    
#print(doubleSortedOutputText)

if outputText == doubleSortedOutputText:
    print("Double selection sort implementation test passed")
else:
    print("Double selection sort implementation test failed")

print(f"Double selection sort performance: {doubleSelectionEndTime - doubleSelectionStartTime}")
print(f"Python \"sorted\" performance: {endTime - startTime}")