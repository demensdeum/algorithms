var wordsList = ["zabbi","e","b","d","c","xyrax","aaa"]

print(wordsList)

for i in 0..<wordsList.count {
    let word = wordsList[i]
    print("---\nword: \(word)\n")
    for y in (0..<i).reversed() {
        let reversedItem = wordsList[y]
        print("y: \(y)")
        print("reversedItem: \(reversedItem)")
        if word < reversedItem {
            let removeIndex = y + 1
            let insertIndex = y
            print("remove at: \(removeIndex)")
            print("insert at: \(insertIndex)")
            print(wordsList)
            wordsList.remove(at: removeIndex)
            wordsList.insert(word, at: insertIndex)
            print(wordsList)
        }
        else {
            break
        }
        print(".")
    }
}

print(wordsList)
