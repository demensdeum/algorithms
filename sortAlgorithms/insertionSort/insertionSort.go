package main

import "fmt"

func main() {
	numbers := []int{8, 99, 11, 3, 2, 1, 3, 34, 11, 3, 8, 512}
	for i := 1; i < len(numbers); i++ {
		for x := i; x >= 1; x-- {
			if numbers[x] < numbers[x-1] {
				var less = numbers[x]
				numbers[x] = numbers[x-1]
				numbers[x-1] = less
			} else {
				break
			}
		}
	}
	fmt.Println(numbers)
}
