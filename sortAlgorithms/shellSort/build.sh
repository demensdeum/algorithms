#!/bin/bash

mypy shellSort.py
if ! [ $? -eq 0 ]
then
    exit 1
fi

python3 shellSort.py