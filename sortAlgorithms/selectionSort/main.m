#include "SelectionSort.h"
#include <Foundation/Foundation.h>

int main (void)
{
    @autoreleasepool {
        id algorithm = [SelectionSort new];
        id numbers = @[@29, @49, @66, @35, @7, @12, @80];
        id mutableNumbers = [NSMutableArray new];
        [mutableNumbers addObjectsFromArray: numbers];
        [algorithm performSort: mutableNumbers];
        [algorithm release];
    }
}