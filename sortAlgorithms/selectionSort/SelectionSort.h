#include <Foundation/Foundation.h>

@interface SelectionSort: NSObject
- (void)performSort:(NSMutableArray<NSNumber *> *)numbers;
@end
