import kotlin.math.min

fun main(args: Array<String>) {
    val searchString = "adasDATAHEADER??jpjjwerthhkjbcvkDATAHEADER??kkasdf"
    searchLongestRepeatedSubstring(searchString)
}

fun longestPrefix(lhs: String, rhs: String): String {
    val maximalLength = min(lhs.length-1, rhs.length -1)
    for (i in 0..maximalLength) {
        val xChar = lhs.take(i)
        val yChar = rhs.take(i)
        if (xChar != yChar) {
            return lhs.substring(0, i-1)
        }
    }
    return lhs.substring(0,maximalLength)
}

fun searchLongestRepeatedSubstring(searchString: String): String {
    val suffixTree = suffixArray(searchString)
    val sortedSuffixTree = suffixTree.sorted()

    var longestRepeatedSubstring = ""
    for (i in 0..sortedSuffixTree.count() - 2) {
        val lhs = sortedSuffixTree[i]
        val rhs = sortedSuffixTree[i+1]
        val longestPrefix = longestPrefix(lhs, rhs)
        if (longestRepeatedSubstring.length < longestPrefix.length) {
            longestRepeatedSubstring = longestPrefix
        }
    }
    return longestRepeatedSubstring
}

fun suffixArray(string: String): Array<String> {
    var suffixArray = ArrayList<String>()
    for (i in 0..string.length - 1) {
        val substring = string.substring(i)
        suffixArray.add(substring)
    }
    return suffixArray.toTypedArray()
}