func lexiCompare(_ lhs: String, _ rhs: String) -> Int {
    let lhs = Array(lhs.unicodeScalars)
    let rhs = Array(rhs.unicodeScalars)
    
    print(lhs)
    
    for i in 0...lhs.count {
        if i >= rhs.count {
            return -1
        }
        let lhsSymbol = lhs[i].value
        let rhsSymbol = rhs[i].value
        
        if lhsSymbol < rhsSymbol {
            return -1
        }
        else if lhsSymbol > rhsSymbol {
            return 1
        }
        else {
            continue
        }
    }
    
    return 0
}

let lhs = "aaa"
let rhs = "ccc"

let result = lexiCompare(lhs, rhs)
print(result)
